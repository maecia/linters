# Maecia Coding Style #

Pour coder avec style ©Franky.

## Les linters utilisés par Maecia ##

### Javascript : ESLINT

### PHP : PHPCS

### CSS : CSSLINT

## Installer les linters sur vos IDE ##

### Sublime Text 3 ###

La façon la plus simple et la plus propre d'utiliser vos linters sur ST3 est d'installer le plugin [SublimeLinter](https://packagecontrol.io/packages/SublimeLinter).

La documentation de SublimeLinter est disponibe sur [https://sublimelinter.readthedocs.io/en/latest/index.html](https://sublimelinter.readthedocs.io/en/latest/index.html "https://sublimelinter.readthedocs.io").

| Linter  |  URL |
|---|---|
| **ESLINT**  |  [https://github.com/roadhump/SublimeLinter-eslint](https://github.com/roadhump/SublimeLinter-eslint "https://github.com/roadhump/SublimeLinter-eslint") |
| **PHPCS**  | [https://github.com/SublimeLinter/SublimeLinter-phpcs](https://github.com/SublimeLinter/SublimeLinter-phpcs "https://github.com/SublimeLinter/SublimeLinter-phpcs")  |
| **CSSLINT**  | [https://github.com/SublimeLinter/SublimeLinter-csslint](https://github.com/SublimeLinter/SublimeLinter-csslint "https://github.com/SublimeLinter/SublimeLinter-csslint")  |

